public class Bai22calMonth {
    public int calMonthRecursion(double money, double rate,int n) {
        double amount = money * Math.pow(1 +  rate, n);
        if (amount - money > money) {
            return n;
        }
        return calMonthRecursion(money, rate,n+1);
    }

    public int callMonth(double money, double rate) {
        int n = 0;
        double amount = 0;
        do {
            n++;
            amount = money * Math.pow(1 + rate , n);
        } while (amount - money < money);
        return n;
    }
}
