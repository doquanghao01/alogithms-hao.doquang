import Model.Product;

import java.util.ArrayList;

public class Bai16maxByPrice {
    public Product maxByPrice(ArrayList<Product> productList) {
        int max = productList.get(0).getPrice();
        int position = 0;
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getPrice() > max) {
                max = productList.get(i).getPrice();
                position = i;
            }
        }

        return productList.get(position);
    }

    public void Bai16maxByPrice() {
        Product product = maxByPrice(Main.listProduct);
        System.out.println("\tName: " + product.getName());
        System.out.println("\tPrice: " + product.getPrice());
        System.out.println("\tQuality: " + product.getQuality());
        System.out.println("\tCategoryId: " + product.getCatedgoryId());

    }

}
