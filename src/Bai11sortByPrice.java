import Model.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai11sortByPrice {
    public List<Product> sortByPrice(ArrayList<Product> productList) {
        for (int i = 0; i < productList.size() - 1; i++) {
            for (int j = 0; j < productList.size() - i - 1; j++) {
                if (productList.get(j).getPrice() > productList.get(j + 1).getPrice()) {
                    Product product = productList.get(j);
                    productList.set(j, productList.get(j + 1));
                    productList.set(j + 1, product);
                }
            }
        }
        return productList;
    }

    public void Bai11sortByPrice() {
        int i = 1;
        for (Product item : sortByPrice(Main.listProduct)
        ) {
            System.out.println("Thông tin sản phẩm thứ " + i++ + " :");
            System.out.println("\tName: " + item.getName());
            System.out.println("\tPrice: " + item.getPrice());
            System.out.println("\tQuality: " + item.getQuality());
            System.out.println("\tCategoryId: " + item.getCatedgoryId());
        }
    }
}
