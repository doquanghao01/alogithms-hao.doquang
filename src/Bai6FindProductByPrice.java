import Model.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai6FindProductByPrice {
    public List<Product> findProductByPrice(ArrayList<Product> listProduct, int price) {
        ArrayList<Product> productArrayList = new ArrayList<>();
        for (Product item : listProduct) {
            if (item.getPrice() <= price) {
                productArrayList.add(item);
            }
        }
        return productArrayList;
    }
    public void Bai6FindProductByPrice() {
        int i = 1;
        int price = 750;
        for (Product item : findProductByPrice(Main.listProduct, price)
        ) {
            System.out.println("Thông tin sản phẩm thứ " + i++ + " :");
            System.out.println("\tName: " + item.getName());
            System.out.println("\tPrice: " + item.getPrice());
            System.out.println("\tQuality: " + item.getQuality());
            System.out.println("\tCategoryId: " + item.getCatedgoryId());
        }
    }
}

