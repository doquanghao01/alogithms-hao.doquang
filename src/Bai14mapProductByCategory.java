import Model.Category;
import Model.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai14mapProductByCategory {
    public List<Product> mapProductByCategory(ArrayList<Product> productList, ArrayList<Category> categoryList) {
        for (Product itemProduct : productList) {
            for (Category itemCategory : categoryList) {
                if (itemCategory.getId() == itemProduct.getCatedgoryId()) {
                    itemProduct.setNameCategory(itemCategory.getName());
                }
            }
        }
        return productList;
    }

    public void Bai14mapProductByCategory() {
        int i = 1;
        for (Product item : mapProductByCategory(Main.listProduct, Main.arrayCategory)
        ) {
            System.out.println("Thông tin sản phẩm thứ " + i++ + " :");
            System.out.println("\tName: " + item.getName());
            System.out.println("\tNameCategory: " + item.getNameCategory());
            System.out.println("\tPrice: " + item.getPrice());
            System.out.println("\tQuality: " + item.getQuality());
            System.out.println("\tCategoryId: " + item.getCatedgoryId());
        }
    }
}
