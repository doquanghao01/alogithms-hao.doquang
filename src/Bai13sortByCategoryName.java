import Model.Category;
import Model.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai13sortByCategoryName {
    public List<Product> sortByCategoryName(ArrayList<Product> productList, ArrayList<Category> categoryList) {
        ArrayList<Product> productArrayList = new ArrayList<>();
        int j;
        Category category;
        for (int i = 0; i < categoryList.size(); i++) {
            category = categoryList.get(i);
            j = i - 1;
            while (j >= 0 && categoryList.get(j).getName().compareTo(category.getName()) > 0) {
                categoryList.set(j + 1, categoryList.get(j));
                j = j - 1;
            }
            categoryList.set(j + 1, category);
        }

        for (Product itemProduct : productList) {
            for (Category itemCategory : categoryList) {
                if (itemCategory.getId() == itemProduct.getCatedgoryId()) {
                    productArrayList.add(itemProduct);
                }
            }
        }
        return productArrayList;
    }

    public void Bai13sortByCategoryName() {
        int i = 1;
        for (Product item : sortByCategoryName(Main.listProduct, Main.arrayCategory)
        ) {
            System.out.println("Thông tin sản phẩm thứ " + i++ + " :");
            System.out.println("\tName: " + item.getName());
            System.out.println("\tPrice: " + item.getPrice());
            System.out.println("\tQuality: " + item.getQuality());
            System.out.println("\tCategoryId: " + item.getCatedgoryId());
        }
    }
}
