public class Bai25Stack {
    private int []array;
    private int SIZE;
    private int topIndex;

    public Bai25Stack(int SIZE) {
        this.array = new int[SIZE];
        this.SIZE = SIZE;
        this.topIndex = -1;
    }

    public void push(int value){
        if (topIndex!=SIZE-1){
            topIndex++;
            array[topIndex]=value;
        }
    }
    public int get(){
        int value=-1;
        if(topIndex>=0){
            value=array[topIndex];
            topIndex--;
        }
        return value;
    }

    public void show(){
        if (topIndex<0){
            System.out.println("Stack trống");
        }else {
            for (int i=0;i<=topIndex;i++){
                System.out.print(array[i]+"");
            }
            System.out.println();
        }
    }
    public void getStack(){
        push(1);
        push(2);
        push(3);
        push(4);
        show();

        get();
        show();
        get();
        show();
        get();
        show();
    }

}

