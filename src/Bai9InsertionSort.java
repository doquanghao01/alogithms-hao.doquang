public class Bai9InsertionSort {
    public void Bai9InsertionSort(int array[])
    {
        int n = array.length;
        int i, key, j;
        for (i = 1; i < n; i++)
        {
            key = array[i];
            j = i-1;
            while (j >= 0 && array[j] > key)
            {
                array[j+1] = array[j];
                j = j-1;
            }
            array[j+1] = key;
        }
    }
}
