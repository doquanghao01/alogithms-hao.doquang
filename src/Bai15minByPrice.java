import Model.Product;

import java.util.ArrayList;
import java.util.Collections;

public class Bai15minByPrice {
    public Product minByPrice(ArrayList<Product> productList) {
        int min = productList.get(0).getPrice();
        int position = 0;
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getPrice() < min) {
                min = productList.get(i).getPrice();
                position = i;
            }
        }

        return productList.get(position);
    }

    public void Bai15minByPrice() {
        Product product = minByPrice(Main.listProduct);
        System.out.println("\tName: " + product.getName());
        System.out.println("\tPrice: " + product.getPrice());
        System.out.println("\tQuality: " + product.getQuality());
        System.out.println("\tCategoryId: " + product.getCatedgoryId());

    }

}
