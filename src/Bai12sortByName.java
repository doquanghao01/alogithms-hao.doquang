import Model.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai12sortByName {

    public List<Product> sortByName(ArrayList<Product> productList) {
        int j;
        Product product;
        for (int i = 0; i < productList.size(); i++) {
            product = productList.get(i);
            j = i - 1;
            while (j >= 0 && productList.get(j).getName().length() > product.getName().length()) {
                productList.set(j + 1, productList.get(j));
                j = j - 1;
            }
            productList.set(j + 1, product);

        }
        return productList;
    }

    public void Bai12sortByName() {
        int i = 1;
        for (Product item : sortByName(Main.listProduct)
        ) {
            System.out.println("Thông tin sản phẩm thứ " + i++ + " :");
            System.out.println("\tName: " + item.getName());
            System.out.println("\tPrice: " + item.getPrice());
            System.out.println("\tQuality: " + item.getQuality());
            System.out.println("\tCategoryId: " + item.getCatedgoryId());
        }
    }
}
