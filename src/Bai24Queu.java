public class Bai24Queu {
    public class Node{
        int vlaue;
        Node next;
        Node(int vlaue){
            this.vlaue=vlaue;
        }
    }
    Node headNode,tailNode;
    public Bai24Queu() {
        headNode=tailNode=null;
    }
    public void push(int value){
        Node newNode=new Node(value);
        if(headNode==null&&tailNode==null){
            headNode=tailNode=newNode;
        }else {
            tailNode.next=newNode;
            tailNode=newNode;
        }
    }

    public int get(){
        if(headNode==null&&tailNode==null){
           return -1;
        }
        int value=headNode.vlaue;
        if(headNode==tailNode){
            headNode=tailNode=null;
        }else {
            headNode=headNode.next;
        }
        return value;
    }
    public void show(){
       if(headNode==null&&tailNode==null){
           System.out.println("Queue rỗng!");
           return;
       }
       Node curNode=headNode;
       while (curNode!=null){
           System.out.print(curNode.vlaue+"");
           curNode=curNode.next;
       }
        System.out.println();
    }
    public void getQueue(){
        push(1);
        push(2);
        push(3);
        show();
        get();
        show();
        get();
        show();
        get();
        show();

    }
}
