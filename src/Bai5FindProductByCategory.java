import Model.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai5FindProductByCategory {
    public List<Product> findProductByCategory(ArrayList<Product> listProduct, int categoryId) {
        ArrayList<Product> productArrayList = new ArrayList<>();
        for (Product item : listProduct) {
            if (item.getCatedgoryId() == categoryId) {
                productArrayList.add(item);
            }
        }
        return productArrayList;
    }
    public void Bai5FindProductByCategory() {
        int i=1;
        int scategoryId = 1;
        for (Product item : findProductByCategory(Main.listProduct, scategoryId)
        ) {
            System.out.println("Thông tin sản phẩm thứ " + i++ + " :");
            System.out.println("\tName: " + item.getName());
            System.out.println("\tPrice: " + item.getPrice());
            System.out.println("\tQuality: " + item.getQuality());
            System.out.println("\tCategoryId: " + item.getCatedgoryId());
        }
    }
}
