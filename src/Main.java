import Model.Category;
import Model.Menu;
import Model.Product;

import java.util.ArrayList;
import java.util.Scanner;

class Main {
    public static ArrayList<Category> arrayCategory;
    public static ArrayList<Product> listProduct;
    public static ArrayList<Menu> menuArrayList=new ArrayList<>();
    public static Scanner scanner = new Scanner(System.in);
    static Bai1giaithuat bai1giaithuat = new Bai1giaithuat();
    static Bai4FindProduct bai4FindProduct = new Bai4FindProduct();
    static Bai5FindProductByCategory bai5FindProductByCategory = new Bai5FindProductByCategory();
    static Bai6FindProductByPrice bai6FindProductByPrice = new Bai6FindProductByPrice();
    static Bai8BubbleSort bai8BubbleSort = new Bai8BubbleSort();
    static Bai9InsertionSort bai9InsertionSort = new Bai9InsertionSort();
    static Bai10SelectionSort bai10SelectionSort = new Bai10SelectionSort();
    static Bai11sortByPrice bai11sortByPrice = new Bai11sortByPrice();
    static Bai12sortByName bai12sortByName = new Bai12sortByName();
    static Bai13sortByCategoryName bai13sortByCategoryName = new Bai13sortByCategoryName();
    static Bai14mapProductByCategory bai14mapProductByCategory = new Bai14mapProductByCategory();
    static Bai15minByPrice bai15minByPrice = new Bai15minByPrice();
    static Bai16maxByPrice bai16maxByPrice = new Bai16maxByPrice();
    static Bai21calSalary bai21calSalary = new Bai21calSalary();
    static Bai22calMonth bai22calMonth = new Bai22calMonth();
    static Bai23printMenu bai23printMenu=new Bai23printMenu();
    static Bai25Stack bai25Stack=new Bai25Stack(3);
    static Bai24Queu bai24Queu=new Bai24Queu();
    public static void getarrayCategory() {
        arrayCategory = new ArrayList<>();
        arrayCategory.add(new Category(1, "Comuter"));
        arrayCategory.add(new Category(2, "Memory"));
        arrayCategory.add(new Category(3, "Card"));
        arrayCategory.add(new Category(4, "Acsesory"));
    }

    public static void getarrayProduct() {
        listProduct = new ArrayList<>();
        listProduct.add(new Product("CPU", 750, 10, 1));
        listProduct.add(new Product("RAM", 50, 2, 2));
        listProduct.add(new Product("HDD", 70, 1, 2));
        listProduct.add(new Product("Main", 400, 3, 1));
        listProduct.add(new Product("Keyboard", 30, 8, 4));
        listProduct.add(new Product("Mouse", 25, 50, 4));
        listProduct.add(new Product("VGA", 60, 35, 3));
        listProduct.add(new Product("Monitor", 120, 28, 2));
        listProduct.add(new Product("Case", 120, 28, 5));
    }
    public static void getMenuArrayList(){
        menuArrayList.add(new Menu(1,"Thể thao",0));
        menuArrayList.add(new Menu(2,"Xã hội",0));
        menuArrayList.add(new Menu(3,"Thể thao trong nước",1));
        menuArrayList.add(new Menu(4,"Giao thông",2));
        menuArrayList.add(new Menu(5,"Môi trường",2));
        menuArrayList.add(new Menu(6,"Thể thao quốc tế",1));
        menuArrayList.add(new Menu(7,"Môi trường đô thị",5));
        menuArrayList.add(new Menu(8,"Giao thông ùn tắc",4));
    }

    public static void main(String[] args) {
        int a[] = {1, 4, 3, 5, 8, 10, 9};
        getarrayCategory();
        getarrayProduct();
        getMenuArrayList();
        System.out.println("Nhập số :");
        String so = scanner.next();
        switch (so) {
            case "1":
                bai1giaithuat.giaithuat();
                break;
            case "2":
                bai4FindProduct.Bai4FindProduct();
                break;
            case "3":
                bai5FindProductByCategory.Bai5FindProductByCategory();
                break;
            case "4":
                bai6FindProductByPrice.Bai6FindProductByPrice();

                break;
            case "5":
                bai8BubbleSort.Bai8BubbleSort(a);
                for (int i = 0; i < a.length; ++i)
                    System.out.print(a[i] + " ");
                break;
            case "6":
                bai9InsertionSort.Bai9InsertionSort(a);
                for (int i = 0; i < a.length; ++i)
                    System.out.print(a[i] + " ");
                break;
            case "7":
                bai10SelectionSort.Bai10SelectionSort(a);
                for (int i = 0; i < a.length; ++i)
                    System.out.print(a[i] + " ");
                break;
            case "8":
                bai11sortByPrice.Bai11sortByPrice();
                break;
            case "9":
                bai12sortByName.Bai12sortByName();
                break;
            case "10":
                bai13sortByCategoryName.Bai13sortByCategoryName();
                break;
            case "11":
                bai14mapProductByCategory.Bai14mapProductByCategory();
                break;
            case "12":
                bai15minByPrice.Bai15minByPrice();
                break;
            case "13":
                bai16maxByPrice.Bai16maxByPrice();
                break;
            case "14":
                System.out.println("Đệ quy: " + bai21calSalary.calSalaryRecursion(20000, 5));
                System.out.println("Ko đệ quy: " + bai21calSalary.calSalary(20000, 5));
                break;
            case "15":
                System.out.println("Đệ quy: " + bai22calMonth.calMonthRecursion(10000, 0.05,0));
                System.out.println("Ko đệ quy: " + bai22calMonth.callMonth(10000, 0.06));
                break;
            case "16":
                bai23printMenu.Bai23printMenu();
                break;
            case "17":
                bai24Queu.getQueue();
                break;
            case "18":
                bai25Stack.getStack();
                break;
            default:
                System.out.println("Vui lòng nhập lại đúng số !");
        }
    }
}
