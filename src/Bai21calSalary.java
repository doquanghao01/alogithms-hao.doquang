public class Bai21calSalary {
    public double calSalaryRecursion(double salary, int n){
        if (n<1){
            return salary;
        }
        return calSalaryRecursion(salary*1.1,n-1);
    }
    public double calSalary(double salary,int n){
        for (int i=0;i<n;i++){
            salary=salary*1.1;
        }
        return salary;
    }
}
